// In order to find the cipher text, you must click letters in a correct sequence.
var buttonLast = "", // Which letter was last hit.
  buttonNumber = 0x0; // How many letters in sequence were pressed.

// This sets up a mediaSession if the browser supports it.
"mediaSession" in navigator &&
  (navigator.mediaSession.metadata = new MediaMetadata({
    title: "Unforgettable",
    artist: "Nat King Cole",
    album: "The Ultimate Collection (Remastered)",
    artwork: [
      { src: "https://dummyimage.com/96x96", sizes: "96x96", type: "image/png" },
      { src: "https://dummyimage.com/128x128", sizes: "128x128", type: "image/png" },
      { src: "https://dummyimage.com/192x192", sizes: "192x192", type: "image/png" },
      { src: "https://dummyimage.com/256x256", sizes: "256x256", type: "image/png" },
      { src: "https://dummyimage.com/384x384", sizes: "384x384", type: "image/png" },
      { src: "https://dummyimage.com/512x512", sizes: "512x512", type: "image/png" },
    ],
  }));

// Clicking `W` starts the sequence.
function Wbutton() {
  if (buttonLast === "") {
    buttonLast = "W";
    buttonNumber += 1;
  } else {
    buttonLast = "";
    buttonNumber = 0;
  }
}

// Clicking `i` is the second letter.
function ibutton() {
  if (buttonLast === "W") {
    buttonLast = "i";
    buttonNumber += 1;
  } else {
    buttonLast = "";
    buttonNumber = 0;
  }
}

// Clicking `n` is the third and fourth letter.
// So this checks if you've hit it twice as well.
function nbutton() {
  if (buttonLast === "i") {
    buttonLast = "n1";
    buttonNumber += 1;
  } else {
    if (buttonLast === "n1") {
      buttonLast = "n2";
      buttonNumber += 1;
    } else {
      buttonLast = "";
      buttonNumber = 0;
    }
  }
}

// Clicking `e` is the fifth letter.
function ebutton() {
  if (buttonLast === "n2") {
    buttonLast = "e";
    buttonNumber += 1;
  } else {
    buttonLast = "";
    buttonNumber = 0;
  }
}

// Clicking `r` is the sixth and last letter.
function rbutton() {
  if (buttonLast === "e" && buttonNumber === 5) {
    // The ciphered text is placed in the hint element now if you've hit the
    // correct sequence.
    var hintDivElement = document.getElementById("hint");
    hintDivElement.innerHTML = "prrwqeyceejdrldrxidr!";
  } else {
    buttonLast = "";
    buttonNumber = 0;
  }
}

// Clicking `w` is the first letter. This works the same as `W` (i.e., is saved
// as uppercase even if clicking a lowercase `w`).
function wbutton() {
  if (buttonLast === "") {
    buttonLast = "W";
    buttonNumber += 1;
  } else {
    buttonLast = "";
    buttonNumber = 0;
  }
}

// All remaining letters just reset the sequence.
function hbutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function tbutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function abutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function ybutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function obutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function ubutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function sbutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function kbutton() {
  buttonLast = "";
  buttonNumber = 0;
}

function lbutton() {
  buttonLast = "";
  buttonNumber = 0;
}

// This is called by clicking on the "Click for Answer" text.
// It places a random misspelled show name on the hint element.
function move() {
  var hintDivElement = document.getElementById("hint"),
    choice = Math.floor(Math.random() * 10);
  switch (choice) {
    case 0:
      hintDivElement.innerHTML = "Batman the Animatied Seirws (1992)";
      break;
    case 1:
      hintDivElement.innerHTML = "Jojo's Biozaree Adventure (2012)";
      break;
    case 2:
      hintDivElement.innerHTML = "Assiasination Classroom (2013)";
      break;
    case 3:
      hintDivElement.innerHTML = "Parasite the Maxim (2014)";
      break;
    case 4:
      hintDivElement.innerHTML = "Neon Gensis Evangelon (1995)";
      break;
    case 5:
      hintDivElement.innerHTML = "Futurmama (1999)";
      break;
    case 6:
      hintDivElement.innerHTML = "X-Meu (1992)";
      break;
    case 7:
      hintDivElement.innerHTML = "Smaurai Kack (2001)";
      break;
    case 8:
      hintDivElement.innerHTML = "Scooby-Doo! Mysftewy Incoroprated (2010)";
      break;
    case 9:
      hintDivElement.innerHTML = "Kll la Kill (2013)";
      break;
    default:
  }
}
